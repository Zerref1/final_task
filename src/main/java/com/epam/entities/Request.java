package com.epam.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Request {
    private Long id;
    private LocalDate bookedFrom;
    private LocalDate bookedTo;
    private Room room;
    private User user;
    private RequestStatus requestStatus;
}
