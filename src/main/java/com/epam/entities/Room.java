package com.epam.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Room {
    private Long id;
    private int numberOfSeats;
    private Status status;
    private TypeOfRoom typeOfRoom;
    private BigDecimal price;
}