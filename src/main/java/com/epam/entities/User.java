package com.epam.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User{
    private Long id;
    private String name;
    private String surname;
    private String patronymic;
    private String login;
    private String password;
    private Role role;
}