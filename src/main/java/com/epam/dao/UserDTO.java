package com.epam.dao;

import com.epam.entities.Role;
import lombok.*;

@Getter
@AllArgsConstructor
@EqualsAndHashCode
public class UserDTO {
    private Long id;
    private String name;
    private String surname;
    private String patronymic;
    private Long requestCount;

    public UserDTO(Long id, String name, String surname, String patronymic) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
    }
}
