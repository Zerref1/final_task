package com.epam.dao.interfaces;

import com.epam.entities.Request;

import java.util.List;
import java.util.Optional;

public interface RequestDAO extends GenericDAO<Request , Long> {
    Optional<Request> findByName(String name);
    List<Request> findRequestByUserId(Long id);
    Boolean updateInformationAboutPayment(Long id);
    Integer countOfRequests(Long id);
}
