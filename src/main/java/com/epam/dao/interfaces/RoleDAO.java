package com.epam.dao.interfaces;

import com.epam.entities.Role;

public interface RoleDAO extends GenericDAO<Role , Long>{
}
