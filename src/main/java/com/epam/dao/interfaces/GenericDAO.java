package com.epam.dao.interfaces;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;


public interface GenericDAO<T , ID> {
    Optional<T> findById(ID id);
    List<T> findAll() throws SQLException;
    T createNew(T entity);
    T update (T entity, ID id);
    void deleteById(ID id);
    void delete(T entity);
}
