package com.epam.dao.interfaces;

import com.epam.entities.User;

import java.util.Optional;

public interface UserDAO extends GenericDAO <User, Long> {

    Optional<User> findByName(String name);
}
