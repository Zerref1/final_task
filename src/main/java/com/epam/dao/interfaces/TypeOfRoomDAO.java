package com.epam.dao.interfaces;

import com.epam.entities.TypeOfRoom;

public interface TypeOfRoomDAO extends GenericDAO <TypeOfRoom, Long> {
}
