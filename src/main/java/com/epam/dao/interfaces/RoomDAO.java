package com.epam.dao.interfaces;

import com.epam.entities.Room;

import java.util.List;
import java.util.Optional;

public interface RoomDAO extends GenericDAO <Room, Long> {
    List<Room> findFreeRooms ();
    Boolean updateRoomStatusById(Long id);
}
