package com.epam.dao.impl;

import com.epam.dao.interfaces.UserDAO;
import com.epam.db.DbManager;
import com.epam.entities.Role;
import com.epam.entities.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class UserDAOImpl implements UserDAO {

    public static final String INSERT_INTO_USER_NAME_SURNAME_PATRONYMIC_LOGIN_PASSWORD_ROLE_VALUES_2 = "INSERT INTO user (name , surname , patronymic , login , password , role_id) VALUES (? , ? , ? , ? , ? , 2 )";
    public static final String SELECT_U_ID_U_NAME_U_SURNAME_U_PATRONYMIC_U_LOGIN_U_PASSWORD_R_ID_AS_ROLE_ID_R_NAME_AS_ROLE_FROM_USER_U_JOIN_ROLE_R_ON_U_ROLE_ID_R_ID = "SELECT u.id, u.name , u.surname , u.patronymic, u.login ,\n" +
            "u.password, r.id as role_id , r.name as role FROM user u \n" +
            "JOIN role r ON u.role_id = r.id\n" +
            "where u.login = ?";
    private Connection connection;
    private Object User;

    public UserDAOImpl() {
        this.connection = DbManager.getConnection();
    }


    @Override
    public Optional<User> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public User createNew(User entity) {
        String sql = INSERT_INTO_USER_NAME_SURNAME_PATRONYMIC_LOGIN_PASSWORD_ROLE_VALUES_2;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setString(2, entity.getSurname());
            preparedStatement.setString(3, entity.getPatronymic());
            preparedStatement.setString(4, entity.getLogin());
            preparedStatement.setString(5, entity.getPassword());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    @Override
    public Optional<User> findByName(String name) {
        String sql = SELECT_U_ID_U_NAME_U_SURNAME_U_PATRONYMIC_U_LOGIN_U_PASSWORD_R_ID_AS_ROLE_ID_R_NAME_AS_ROLE_FROM_USER_U_JOIN_ROLE_R_ON_U_ROLE_ID_R_ID;
        User user = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = new User(
                        resultSet.getLong("id"),
                        resultSet.getString("name"),
                        resultSet.getString("surname"),
                        resultSet.getString("patronymic"),
                        resultSet.getString("login"),
                        resultSet.getString("password"),
                        new Role(resultSet.getLong("role_id"),
                                resultSet.getString("role")));
            }
            return Optional.ofNullable(user);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public User update(User entity, Long aLong) {
        return null;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(User entity) {

    }
}
