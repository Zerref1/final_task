package com.epam.dao.impl;

import com.epam.dao.interfaces.RequestDAO;
import com.epam.db.DbManager;
import com.epam.entities.*;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;

public class RequestDAOImpl implements RequestDAO {

    public static final String Querry_For_Getting_All_Info_About_Request = "SELECT \n" +
            "    r.id as requestId , r.booked_from as bookedFrom , r.booked_to as bookedTo , \n" +
            "    user.id as userId, user.name , user.surname , user.patronymic , user.login, user.password , user.role_id as roleId,\n" +
            "    room.id as roomId , room.price , room.number_of_seats as numberOfSeats,\n" +
            "    type_of_room.name as typeOfRoom,  \n" +
            "    request_status.id as requestStatusId,\n" +
            "    request_status.name as requestStatus\n" +
            "from request r\n" +
            "join user on r.user_id = user.id\n" +
            "join room on r.room_id = room.id\n" +
            "join type_of_room on room.type_of_room_id = type_of_room.id\n" +
            "join request_status on r.request_status_id = request_status.id";
    public static final String QUERY_FOR_CREATING_NEW_REQUEST = "Insert into request \n" +
            "(booked_from, \n" +
            "booked_to,\n" +
            "user_id,\n" +
            "room_id,\n" +
            "request_status_id)\n" +
            "values (? , ? , ? , ? , 2)";
    public static final String FIND_ALL_REQUESTS_BY_USER_ID = "SELECT r.id as requestId , r.booked_from as bookedFrom , r.booked_to as bookedTo ,\n" +
            "\tuser.id as userId,\n" +
            "\troom.price , room.number_of_seats as numberOfSeats,\n" +
            "\ttype_of_room.name as typeOfRoom,\n" +
            "\trequest_status.name as requestStatus\n" +
            "from request r\n" +
            "\tjoin user on r.user_id = user.id\n" +
            "\tjoin room on r.room_id = room.id\n" +
            "\tjoin type_of_room on room.type_of_room_id = type_of_room.id\n" +
            "\tjoin request_status on r.request_status_id = request_status.id\n" +
            "where user_id = ?";
    public static final String UPDATE_REQUEST_STATUS_AFTER_PEYMENT = "update request set request_status_id = 1 where id = ?";
    private Connection connection;

    @Override
    public Optional<Request> findById(Long id) {
        return Optional.ofNullable(null);
    }

    public RequestDAOImpl() {
        this.connection = DbManager.getConnection();
    }


    @Override
    public List<Request> findAll() {
        String sql = Querry_For_Getting_All_Info_About_Request;
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        List<Request> requests = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                requests.add(new Request(
                        resultSet.getLong("requestId"),
                        resultSet.getDate("bookedFrom").toLocalDate(),
                        resultSet.getDate("bookedTo").toLocalDate(),
                        new Room(resultSet.getLong("roomId"),
                                resultSet.getInt("numberOfSeats"),
                                null,
                                new TypeOfRoom(null,
                                        resultSet.getString("typeOfRoom")),
                                resultSet.getBigDecimal("price")),
                        new User(resultSet.getLong("userId"),
                                resultSet.getString("name"),
                                resultSet.getString("surname"),
                                resultSet.getString("patronymic"),
                                resultSet.getString("login"), resultSet.getString("password"),
                                null),
                        new RequestStatus(resultSet.getLong("requestStatusId"),
                                resultSet.getString("requestStatus"))));
            }
            return requests;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public Request createNew(Request entity) {
        String sql = QUERY_FOR_CREATING_NEW_REQUEST;
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setDate(1, Date.valueOf(entity.getBookedFrom()));
            preparedStatement.setDate(2, Date.valueOf(entity.getBookedTo()));
            preparedStatement.setLong(3, entity.getUser().getId());
            preparedStatement.setLong(4, entity.getRoom().getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    @Override
    public Request update(Request entity, Long id) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public void delete(Request entity) {

    }

    @Override
    public Optional<com.epam.entities.Request> findByName(String name) {
        return Optional.empty();
    }

    @Override
    public List<Request> findRequestByUserId(Long id) {
        String sql = FIND_ALL_REQUESTS_BY_USER_ID;
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        List<Request> requests = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                requests.add(new Request(
                        resultSet.getLong("requestId"),
                        resultSet.getDate("bookedFrom").toLocalDate(),
                        resultSet.getDate("bookedTo").toLocalDate(),
                        new Room(null,
                                resultSet.getInt("numberOfSeats"),
                                null,
                                new TypeOfRoom(null,
                                        resultSet.getString("typeOfRoom")),
                                resultSet.getBigDecimal("price")),
                        null,
                        new RequestStatus(null,
                                resultSet.getString("requestStatus"))));
            }
            return requests;
        } catch (SQLException e) {
            throw new RuntimeException();
        }
    }

    @Override
    public Boolean updateInformationAboutPayment(Long id) {
        String sql = UPDATE_REQUEST_STATUS_AFTER_PEYMENT;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1 ,id);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Integer countOfRequests(Long id) {
        return null;
    }
}
