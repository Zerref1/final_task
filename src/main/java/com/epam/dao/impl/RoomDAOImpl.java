package com.epam.dao.impl;

import com.epam.dao.interfaces.RoomDAO;
import com.epam.db.DbManager;
import com.epam.entities.Room;
import com.epam.entities.Status;
import com.epam.entities.TypeOfRoom;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;

public class RoomDAOImpl implements RoomDAO {
    public static final String Find_Free_Rooms = "SELECT\n" +
            "\tr.id , r.number_of_seats as numberOfSeats,\n" +
            "\ttype_of_room.name as typeOfRoom, r.price,\n" +
            "\tstatus.name as statusName\n" +
            "From room r\n" +
            "join status on r.status_id = status.id\n" +
            "join type_of_room on r.type_of_room_id = type_of_room.id\n" +
            "where status.id = 1\n" +
            "order by r.number_of_seats";
    public static final String UPDATE_STATUS_OD_ROOM = "update room set status_id = 2 where id =  ?";

    public RoomDAOImpl() {
        this.connection = DbManager.getConnection();
    }

    private Connection connection;

    @Override
    public Optional<Room> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public List<Room> findAll() {
        return null;
    }

    @Override
    public Room createNew(Room entity) {
        return null;
    }

    @Override
    public Room update(Room entity, Long aLong) {
        return null;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(Room entity) {

    }

    @Override
    public List<Room> findFreeRooms() {
        String sql = Find_Free_Rooms;
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        Room room = null;
        List<Room> rooms = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                rooms.add(new Room(
                        resultSet.getLong("id"),
                        resultSet.getInt("numberOfSeats"),
                        new Status(resultSet.getLong("id"), resultSet.getString("statusName")),
                        new TypeOfRoom(resultSet.getLong("id"), resultSet.getString("typeOfRoom")),
                        resultSet.getBigDecimal("price")));
            }
            return rooms;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Boolean updateRoomStatusById(Long id) {
        String sql = UPDATE_STATUS_OD_ROOM;
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1 ,id);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
