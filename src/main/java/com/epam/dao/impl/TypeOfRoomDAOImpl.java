package com.epam.dao.impl;

import com.epam.dao.interfaces.TypeOfRoomDAO;
import com.epam.db.DbManager;
import com.epam.entities.TypeOfRoom;

import java.awt.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TypeOfRoomDAOImpl implements TypeOfRoomDAO {

    public static final String SELECT_FROM_HOTEL_TYPE_OF_ROOM = "SELECT * FROM hotel.type_of_room";
    private Connection connection;

    public TypeOfRoomDAOImpl() {
        this.connection = DbManager.getConnection();
    }

    @Override
    public Optional<TypeOfRoom> findById(Long aLong) {
        return null;
    }

    @Override
    public List<TypeOfRoom> findAll(){
        String sql = SELECT_FROM_HOTEL_TYPE_OF_ROOM;
        List <TypeOfRoom> typeOfRooms = new ArrayList<>();
        TypeOfRoom typeOfRoom = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)){
            ResultSet resultSet = preparedStatement.executeQuery(sql);
            while (resultSet.next()){
                typeOfRooms.add(new TypeOfRoom(resultSet.getLong("id"),
                        resultSet.getString("name")));
            }return typeOfRooms;
        }catch (SQLException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public TypeOfRoom createNew(TypeOfRoom entity) {
        return null;
    }

    @Override
    public TypeOfRoom update(TypeOfRoom entity, Long aLong) {
        return null;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(TypeOfRoom entity) {

    }
}
