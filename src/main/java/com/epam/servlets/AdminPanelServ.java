package com.epam.servlets;

import com.epam.dao.UserDTO;
import com.epam.dao.impl.RequestDAOImpl;
import com.epam.dao.impl.RoomDAOImpl;
import com.epam.dao.impl.UserDAOImpl;
import com.epam.dao.interfaces.UserDAO;
import com.epam.entities.Request;
import com.epam.entities.Room;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@WebServlet("/admin")
public class AdminPanelServ extends HttpServlet {
    private UserDAO userDAO = new UserDAOImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDAOImpl requestDAOImpl = new RequestDAOImpl();
        List<Request> requests;
        requests = requestDAOImpl.findAll();
        req.setAttribute("request", requests);
        Map<UserDTO, Long> counted = requests.stream()
                .collect(Collectors.groupingBy(request -> new UserDTO(request.getUser().getId(),
                        request.getUser().getName(), request.getUser().getSurname(),
                        request.getUser().getPatronymic()), Collectors.counting()));
        List<UserDTO> collect = counted.keySet().stream().map(userDTO -> new UserDTO(userDTO.getId(),
                userDTO.getName(), userDTO.getSurname(),
                userDTO.getPatronymic(), counted.get(userDTO))).collect(Collectors.toList());
        req.setAttribute("collect" , collect);
        req.getRequestDispatcher("admin.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
