package com.epam.servlets;

import com.epam.dao.impl.RequestDAOImpl;
import com.epam.dao.impl.RoomDAOImpl;
import com.epam.dao.impl.TypeOfRoomDAOImpl;
import com.epam.entities.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.*;

@WebServlet("/order")
public class ChooseFromFreeRoomsServ extends HttpServlet {
    private RequestDAOImpl requestDAO = new RequestDAOImpl();
    private RoomDAOImpl roomDAO = new RoomDAOImpl();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RoomDAOImpl roomDAOimpl = new RoomDAOImpl();
        TypeOfRoomDAOImpl typeOfRooms = new TypeOfRoomDAOImpl();
        List<Room> rooms = roomDAOimpl.findFreeRooms();
        req.setAttribute("rooms" , rooms);
        req.getRequestDispatcher("freeRoom.jsp").forward(req, resp);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String typeOfRoom = req.getParameter("typeOfRoom");
        LocalDate dateFrom = LocalDate.parse(req.getParameter("dateFrom"));
        LocalDate dateTo = LocalDate.parse(req.getParameter("dateTo"));
        BigDecimal price = new BigDecimal(req.getParameter("price"));
        Long roomId = Long.valueOf(req.getParameter("bookId"));
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("user");
        Long userId = user.getId();
        Request request = Request.builder()
                .bookedFrom(dateFrom)
                .bookedTo(dateTo)
                .room(Room.builder().id(roomId).build())
                .user(User.builder().id(userId).build())
                .build();
        requestDAO.createNew(request);
        roomDAO.updateRoomStatusById(roomId);
        System.out.println(request);
        req.getRequestDispatcher("profile.jsp").forward(req, resp);
    }
}
