package com.epam.servlets;

import com.epam.dao.impl.UserDAOImpl;
import com.epam.dao.interfaces.UserDAO;
import com.epam.entities.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/login")
public class LoginServ extends HttpServlet {
    private UserDAO userDAO = new UserDAOImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("Login");
        String password = req.getParameter("Password");
        User user = userDAO.findByName(login).orElseThrow(RuntimeException::new);
        if (user.getLogin().equals(login) && user.getPassword().equals(password)) {
            HttpSession session = req.getSession();
            session.setAttribute("login", user.getLogin());
            session.setAttribute("role", user.getRole().getName());
            session.setAttribute("roleId" , user.getRole().getId());
            session.setAttribute("user", user);
            session.setAttribute("name", user.getName());
            session.setAttribute("surname", user.getSurname());
            session.setAttribute("patronymic", user.getPatronymic());
            session.setAttribute("id" , user.getId());
            PrintWriter out = resp.getWriter();
            resp.sendRedirect("/profile");
            out.close();
        } else {
            resp.sendRedirect("login.jsp");
        }
    }
}
