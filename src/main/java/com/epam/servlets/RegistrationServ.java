package com.epam.servlets;

import com.epam.dao.impl.UserDAOImpl;
import com.epam.dao.interfaces.UserDAO;
import com.epam.entities.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/registration")
public class RegistrationServ extends HttpServlet {

    private UserDAO userDAO = new UserDAOImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("registration.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("Login");
        String password = req.getParameter("Password");
        String name = req.getParameter("Name");
        String surname = req.getParameter("Surname");
        String patronymic = req.getParameter("Patronymic");

        if (login != null && password != null && name != null && surname != null && patronymic != null) {
            User user = User.builder()
                    .login(login).name(name).surname(surname).patronymic(patronymic).password(password).build();
            userDAO.createNew(user);
        } else {
            req.getRequestDispatcher("registration.jsp").forward(req, resp);
            return;
        }
        req.getRequestDispatcher("homePage.jsp").forward(req, resp);
    }
}
