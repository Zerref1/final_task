package com.epam.servlets;


import com.epam.dao.impl.RequestDAOImpl;
import com.epam.dao.impl.UserDAOImpl;
import com.epam.dao.interfaces.UserDAO;
import com.epam.entities.Request;
import com.epam.entities.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/profile")
public class ProfileServ extends HttpServlet {
    private UserDAO userDAO = new UserDAOImpl();
    private RequestDAOImpl requestDAO = new RequestDAOImpl();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("user");
        req.setAttribute("login", user.getLogin());
        RequestDAOImpl requestDAOImpl = new RequestDAOImpl();
        List<Request> requests = new ArrayList<>();
        requests = requestDAOImpl.findRequestByUserId(user.getId());
        requests.forEach(requ -> requ.getRoom().setPrice(new BigDecimal(
                ((requ.getBookedTo().toEpochDay() - requ.getBookedFrom().toEpochDay())*requ.getRoom().getPrice().longValue())
        )));
        req.setAttribute("requests" , requests);
        req.setAttribute("roleId" , session.getAttribute("roleId"));
        req.getRequestDispatcher("profile.jsp").forward(req, resp);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long requestBookId = Long.valueOf(req.getParameter("requestBookId"));
        requestDAO.updateInformationAboutPayment(requestBookId);
        resp.sendRedirect("/profile");
    }
}