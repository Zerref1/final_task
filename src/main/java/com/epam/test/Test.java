package com.epam.test;

import com.epam.db.DbManager;
import com.epam.entities.Role;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        Connection connection = DbManager.getConnection();
        String query = "SELECT * FROM hotel.role";
        List<Role> role = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultset = statement.executeQuery(query);
            while (resultset.next()){
                role.add(new Role(resultset.getLong("id"),
                        resultset.getString("name")));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        System.out.println(role);
    }
}
