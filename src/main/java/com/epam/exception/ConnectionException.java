package com.epam.exception;

public class ConnectionException extends RuntimeException {
    public ConnectionException(String message , Throwable name){
        super(message , name);
    }
}
