<%@ include file="/WEB-INF/directive/taglib.jspf" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script>
        function findByTypeOfRoom(cells) {
            var selec = document.getElementById("myInput");
            var table = document.getElementById("table");
            for (var i = 1; i < table.rows.length; i++) {
                if (selec.value == table.rows[i].cells[cells].innerText.trim()) {
                    table.rows[i].style.display = "";
                } else {
                    table.rows[i].style.display = "none";
                }
            }
        }
        function reset() {
            var table = document.getElementById("table");
            for (var i = 1; i < table.rows.length; i++) {
                table.rows[i].style.display = "";
            }
        }
    </script>
</head>
<body>
<select id="myInput" onchange="findByTypeOfRoom(0)">
    <c:forEach var="rooms" items="${rooms}">
        <option>
            <c:out value="${rooms.getTypeOfRoom().getName()}"></c:out>
        </option>
    </c:forEach>
</select>
<input type="button" onclick="reset()" value="Reset">
<table class="table table-hover" id="table">
    <thead>
    <tr>
        <th scope="col">Type of room</th>
        <th scope="col">Number of seats</th>
        <th scope="col">Price per day</th>
        <th scope="col">Date from</th>
        <th scope="col">Date to</th>
        <th scope="col">Choose room!</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="rooms" items="${rooms}">
        <tr>
            <th scope="row" name="typeOfRoom${rooms.getId()}">
                <c:out value="${rooms.getTypeOfRoom().getName()}"/>
            </th>
            <th scope="row" name="numberOfSeats${rooms.getId()}">
                <c:out value="${rooms.getNumberOfSeats()}"/>
            </th>
            <th scope="row" align="center" name="price${rooms.getId()}">
                <c:out value="${rooms.getPrice()}"></c:out>
            </th>
            <form action="order" method="post">
                <th><input type="date" name="dateFrom" required></th>
                <th><input type="date" name="dateTo" required></th>
                <td width="5%">
                    <input type="hidden" name="bookId" value="${rooms.getId()}">
                    <input type="hidden" name="typeOfRoom" value="${rooms.getTypeOfRoom()}">
                    <input type="hidden" name="numberOfSeats" value="${rooms.getNumberOfSeats()}">
                    <input type="hidden" name="price" value="${rooms.getPrice()}">
                    <input type="hidden" name="requestStatusId" value="${rooms.getPrice()}">
                    <input type="hidden" name="">
                    <input type="submit" value="Book Order">
                </td>
            </form>
        </tr>
    </c:forEach>
    </tbody>
</table>