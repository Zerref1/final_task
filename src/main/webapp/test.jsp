<%@ include file="/WEB-INF/directive/taglib.jspf" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form  action="request" method="post">
    <select name="typeOfRoom" id="typeOfRoom" onchange="check()">
        <c:forEach var="room" items="${typeOfRoom}">
            <option value="${room.getId()}">
                <c:out value="${room.getName()}"/>
            </option>
        </c:forEach>
    </select>
    <select name="numberOfSeats" id="numberOfSeats" onchange="check()">
        <c:forEach var="rooms" items="${numberOfSeats}">
            <option value="${rooms}">
                <c:out value="${rooms.getNumberOfSeats()}"/>
            </option>
        </c:forEach>
    </select>
</form>
</body>
