<%@ include file="/WEB-INF/directive/taglib.jspf" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<body>
<form action="room" method="post">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Date from</th>
            <th scope="col">Date to</th>
            <th scope="col">Room Id</th>
            <th scope="col">Number of seats</th>
            <th scope="col">Type of room</th>
            <th scope="col">User Id</th>
            <th scope="col">Name </th>
            <th scope="col">Surname</th>
            <th scope="col">Patronymic</th>
            <th scope="col">Login</th>
            <th scope="col">Book status</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="requests" items="${request}">
            <tr>
                <td>
                    <c:out value="${requests.getId()}"/>
                </td>
                <td>
                    <c:out value="${requests.getBookedFrom()}"/>
                </td>
                <td>
                    <c:out value="${requests.getBookedTo()}"/>
                </td>
                <td>
                    <c:out value="${requests.getRoom().getId()}"/>
                </td>
                <td>
                    <c:out value="${requests.getRoom().getNumberOfSeats()}"/>
                </td>
                <td>
                    <c:out value="${requests.getRoom().getTypeOfRoom().getName()}"/>
                </td>
                <td>
                    <c:out value="${requests.getUser().getId()}"/>
                </td>
                <td>
                    <c:out value="${requests.getUser().getName()}"/>
                </td>
                <td>
                    <c:out value="${requests.getUser().getSurname()}"/>
                </td>
                <td>
                    <c:out value="${requests.getUser().getPatronymic()}"/>
                </td>
                <td>
                    <c:out value="${requests.getUser().getLogin()}"/>
                </td>
                <td>
                    <c:out value="${requests.getRequestStatus().getName()}"/>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <button><a name="enter" href="/profile" value="Enter">Go back to profile</a></button>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Surname</th>
            <th scope="col">Patronymic</th>
            <th scope="col">Count of requests</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="collect" items="${collect}">
            <tr>
                <th><c:out value="${collect.getName()}"/></th>
                <th><c:out value="${collect.getSurname()}"/></th>
                <th><c:out value="${collect.getPatronymic()}"/></th>
                <th><c:out value="${collect.getRequestCount()}"/></th>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</form>
</body>