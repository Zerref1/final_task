<%@ include file="/WEB-INF/directive/taglib.jspf" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<i> Your login:</i>
<b><c:out value="${login}"/></b><br>
<i>Your name:</i>
<b><c:out value="${name}"/></b><br>
<i>Your surname:</i>
<b><c:out value="${surname}"/></b><br>
<i>Your patronymic:</i>
<b><c:out value="${patronymic}"/></b><br>
<button><a name="enter" href="/logOut" value="Enter">Log out</a></button>
<button><a name="enter" href="/order" value="Enter">Choose from free rooms</a></button>
<c:if test="${roleId == 1}">
    <button><a name="enter" href="/admin" value="Enter">Go to admin panel</a></button>
</c:if>
<c:if test="${not empty requests}">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Request Id</th>
            <th scope="col">Date from</th>
            <th scope="col">Date to</th>
            <th scope="col">Price</th>
            <th scope="col">Number of seats</th>
            <th scope="col">Type of room</th>
            <th scope="col">Request status</th>
            <th scope="col">Payment</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="requests" items="${requests}">
            <tr>
                <th scope="row" name="requestId${requests.getId()}">
                    <c:out value="${requests.getId()}"/>
                </th>
                <th scope="row" name="bookedFrom${requests.getId()}">
                    <c:out value="${requests.getBookedFrom()}"/>
                </th>
                <th scope="row" name="bookedTo${requests.getId()}">
                    <c:out value="${requests.getBookedTo()}"/>
                </th>
                <th scope="row" align="center" name="price${requests.getId()}">
                    <c:out value="${requests.getRoom().getPrice()}"></c:out>
                </th>
                <th scope="row" align="center" name="numberOfSeats${requests.getId()}">
                    <c:out value="${requests.getRoom().getNumberOfSeats()}"></c:out>
                </th>
                <th scope="row" align="center" name="typeOfRoom${requests.getId()}">
                    <c:out value="${requests.getRoom().getTypeOfRoom().getName()}"></c:out>
                </th>
                <th scope="row" align="center" name="requestStatus${requests.getId()}">
                    <c:out value="${requests.getRequestStatus().getName()}"></c:out>
                </th>
                <form action="profile" method="post">
                    <td width="5%">
                        <input type="hidden" name="requestBookId" value="${requests.getId()}">
                        <input type="hidden" name="bookId" value="${rooms.getId()}">
                        <input type="hidden" name="typeOfRoom" value="${rooms.getTypeOfRoom()}">
                        <input type="hidden" name="numberOfSeats" value="${rooms.getNumberOfSeats()}">
                        <input type="hidden" name="price" value="${rooms.getPrice()}">
                        <input type="hidden" name="requestStatusId" value="${rooms.getPrice()}">
                        <input type="hidden" name="">
                        <input type="submit" value="Pay">
                    </td>
                </form>
                </th>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</c:if>

</body>
</html>